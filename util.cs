﻿using System;

namespace TheSpaceGame2
{
	public partial class util //Contains utility methods such as wait
	{
		public static void wait(int num)//Allows you to wait x miliseconds        
		{            
			System.Threading.Thread.Sleep(num);
		}

		public static void checkPlayerDeath () {
			if (PlayerShip.health <= 0) {
				Console.WriteLine ("Game Over");
			}
		}

		public static void PressKey() //Because lazy, amirite?
		{

			Console.WriteLine("Press key to confirm");
			Console.ReadKey();
			Console.WriteLine();
			Console.WriteLine();
		}
		public static void loading () {
			Console.Write(".");
			wait (1000);
			Console.Write(".");
			wait (1000);
			Console.Write (".");
		}

		public static void FlavorText()
		{
			Random gen = new Random();
			int num = gen.Next(1, 11);

			switch(num)
			{
			case 1:
				Console.WriteLine("After a few hours rest, your crew is prepared to move again.");
				break;
			case 2:
				Console.WriteLine("Your crew rests, and give your FTL drive time to recharge.");
				break;
			case 3:
				Console.WriteLine("Up an adam, it's time to move!");
				break;
			case 4:
				Console.WriteLine(globalvar.ShipAI +": It is time to move");
				break;
			case 5:
				Console.WriteLine(globalvar.ShipAI +": There is an asteroid heading towards our ship");
				TheSpaceGame2.EventHandler.Asteroid();
				break;
			case 6:
				Console.WriteLine("It was an uneventful few hours.");
				break;
			case 7:
				Console.WriteLine("Your shield generator malfunctioned suddenly. Your crew resolved the issue, but not before you lost 5% of your shield power.");
				PlayerShip.shieldsCurrent = PlayerShip.shieldsCurrent - 5;
				break;
			case 8:
				Console.WriteLine("Your crew is restless and eager to move on");
				break;
			case 9:
				Console.WriteLine("While you wait, you find a small fuel cell floating in space");
				PlayerShip.fuel = PlayerShip.fuel++;
				break;
			case 10:
				Console.WriteLine("You run into an asteroid belt, with three asteroids on a collision course for your ship!");
				for (int i = 0; i <= 3; i++ )
				{
					TheSpaceGame2.EventHandler.Asteroid();
				}
				break;



			}
		}    

}
}
