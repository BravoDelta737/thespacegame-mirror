﻿using System;

namespace TheSpaceGame2
{

	class RandomNumGen {
		public static Random numberGen = new Random ();
	}

	class PlayerShip {
		public static int shields = 25;
		public static int health = 100;
		public static int torpedocount = 10;
		public static int torpedodamage = RandomNumGen.numberGen.Next (1,11);

	}

	public class EnemyShip {
		public int type;
		public int race;
		public int shields;
		public int health;
		public int torpedocount;
		public int torpedodamage;
	}



	public class Combat {
		public static EnemyShip enemyShip = new EnemyShip ();

		public static Boolean checkPlayerShields () {
			if (PlayerShip.shields > 0) {
				return true;
			} else
				return false;
		}

		public static Boolean checkEnemyShields () {
			if (enemyShip.shields > 0) {
				return true;
			} else {
				return false;
			}
		}

		public static Boolean damageChance () {
			Random randomGen = new Random ();
			int numberGen = randomGen.Next (1, 101);

			if (numberGen < 25) {
				return false;
			} else {
				return true;

			}
		}
		
		
		public static void EnemyTurn () {
			Console.WriteLine ("It's your opponets turn");
			int weponAllowence = 2;
			while (weponAllowence != 0 && PlayerShip.health > 0 && enemyShip.health > 0) {
				
			Random numGen = new Random ();
			int weponChoice = numGen.Next (1,2);

				switch (weponChoice) {
				case 1: 
					Console.WriteLine ("Armed torpedo detected!");
					bool chance = damageChance ();

					if (chance) {
						bool playerShield = checkPlayerShields ();

						if (playerShield) {
							PlayerShip.shields = PlayerShip.shields - enemyShip.torpedodamage;
							weponAllowence--;
							Console.WriteLine ("Torpedo hit!");
							Console.WriteLine ("Shields down to " + PlayerShip.shields + "%");
						} else {
							PlayerShip.health = PlayerShip.health - enemyShip.torpedodamage;
							weponAllowence--;
							Console.WriteLine ("Direct hit!");
							Console.WriteLine ("Hull integrity down to " + PlayerShip.health + "%");
							checkPlayerDeath ();
						}
					} else {
						Console.WriteLine ("Enemy Torpedo Missed!");
						weponAllowence--;
					}
					break;
				}


			}
		}

			


			
		
	

		public static void PlayerTurn () {
			Console.WriteLine ("It's your turn");
			int weponAllowence = 2;
			while (weponAllowence != 0 && PlayerShip.health > 0 && enemyShip.health > 0) {
				Console.WriteLine ();
				Console.WriteLine ("Awaiting your orders");
				string playerChoice = Console.ReadLine ();

				switch (playerChoice) {
				case "fire torpedo":
					Console.WriteLine ("You have " + PlayerShip.torpedocount + " torpedos left");
					bool chance = damageChance ();
					if (chance) {
						bool enShield = checkEnemyShields ();

						if (enShield) {
							enemyShip.shields = enemyShip.shields - PlayerShip.torpedodamage;
							Console.WriteLine ("Hit! Enemy shields are now at " + enemyShip.shields + "%");
							PlayerShip.torpedocount--;
							weponAllowence--;
						} else {
							enemyShip.health = enemyShip.health - PlayerShip.torpedodamage;
							Console.WriteLine ("Direct hit! Enemy hull integrity down to " + enemyShip.health + "%");
							PlayerShip.torpedocount--;
							weponAllowence--;
						}
					} else {
						Console.WriteLine ("Torpedo missed!");
						PlayerShip.torpedocount--;
						weponAllowence--;
					}
					break;
				}
			}
			Console.WriteLine ("End turn");
		}

		public static void checkPlayerDeath () {
			if (PlayerShip.health <= 0) {
				Console.WriteLine ("Game Over");
			}
		}

		public static bool checkHealth () {
			if (PlayerShip.health > 0 && enemyShip.health > 0) {
				return true;
			} else {
				return false;
			}
		}

		public static void CombatLoop () {
			Console.WriteLine ("RED ALERT! Enemy ship approaching. Full power to wepons and shields");
			enemyShip.health = 100;
			enemyShip.shields = 100;
			enemyShip.torpedocount = 10;
			enemyShip.torpedodamage = RandomNumGen.numberGen.Next (1,8);

			while (PlayerShip.health > 0 && enemyShip.health > 0) {
				if (checkHealth ()) {
					PlayerTurn ();
					if (checkHealth ()) {
						EnemyTurn ();
					} else {
						Console.WriteLine ("Enemy ship destroied!");
					}
				} else {
					Console.WriteLine ("Game Over");
				}



			}
		}
	}

	public class TestCombatSystem
	{
		public static void Main (String[] args) {
			Combat.CombatLoop ();
		}
	}
}

