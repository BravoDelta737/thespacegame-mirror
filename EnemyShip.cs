﻿using System;

namespace TheSpaceGame2
{
	public partial class EnemyShip {
		public int type;
		public int race;
		public int shieldsCurrent;
		public int healthCurrent;
		public int healthTotal;
		public int torpedocount;
		public int torpedodamage;
		public int shieldsTotal;
	}
}

