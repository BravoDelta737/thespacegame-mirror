﻿using System;
using TheSpaceGame;

namespace TheSpaceGame2
{
	public partial class PlayerEvents //Used to generate and create player activated events
	{
		public static void AwaitOrders()//The primary place for player input. 
		{

			bool ordering = true;//as long as ordering is true, it'll keep cycling through the orders loop
			while (ordering)
			{


				Console.WriteLine( globalvar.ShipAI + ":  Awaiting your orders captian.");
				string orders = Console.ReadLine();



				switch (orders)
				{

				//FTL drive commands
				case "spin up ftl drive":
					PlayerEvents.jump();
					Console.WriteLine();                        
					break;

				case "ftl drive":
					PlayerEvents.jump();
					Console.WriteLine();                        
					break;
				case "ftl":
					PlayerEvents.jump();
					Console.WriteLine(); 
					break;
				case "ftl jump":
					PlayerEvents.jump();
					Console.WriteLine(); 
					break;
				case "jump":
					PlayerEvents.jump();
					Console.WriteLine(); 
					break;

					//Explore commands
				case "explore":
					PlayerEvents.explore();
					break;

					//Scan Command

					//Map maniputlation
				case "map":
					MapGen.CreateStarChart();
					break;


					/* ONE BIG BALL OF ILL DO IT EVENTUALLY
                    case "show":
                        Console.Write("Select X Coordinate:");
                        int xcord = Convert.ToInt32(Console.Read());

                        Console.Write("Select X Coordinate:");
                        int ycord = Convert.ToInt32(Console.Read());

                        MapGen.ShowCord(xcord,ycord);
                        break;
                    */


					//Ends the orders loop, and returns to the primary gameplay loop to move to the next turn
				case "rest":
					Console.WriteLine("You and your crew are tired. It's time to rest.");
					Console.WriteLine();
					ordering = false;
					break;

				case "end":
					Console.WriteLine("You and your crew are tired. It's time to rest.");
					Console.WriteLine();
					ordering = false;
					break;

				case "end turn":
					Console.WriteLine("You and your crew are tired. It's time to rest.");
					Console.WriteLine();
					ordering = false;
					break;

					//Default Command, if input doesn't match any case
				default:
					Console.WriteLine();
					Console.WriteLine("Invalid command");
					Console.WriteLine();
					ordering = true;
					break;


				}
			}
		}

		private static int promptForChord() {

			string input = Console.ReadLine();
			int chord;

			try{
				chord = Convert.ToInt32(input);
			}
			catch(FormatException) {
				Console.WriteLine ("That's not a number, Cheif...");
				return promptForChord ();
			}

			if (chord > 20 || chord < 0) {
				Console.WriteLine ("Cheif, we can only jump within the 20 lightyear region" +
					" set by the shadow proclamation.");
				return promptForChord ();
			} else {
				return chord;
			}
		}

		public static void jump()
		{
			//Random gen = new Random(); I dont think we need this

			if (globalvar.Jumped)
			{
				Console.WriteLine(globalvar.ShipAI + ": You've already jumped this turn");
			}
			else
			{
				MapGen.CreateStarChart();
				Console.WriteLine(globalvar.ShipAI + ":  Enter destination solution");
				Console.WriteLine("Enter An X Coordinate: ");
				int xco = promptForChord();

				Console.WriteLine("Enter A Y Coordinate");
				int yco = promptForChord();

				PlayerShip.xlocation = xco;
				PlayerShip.ylocation = yco;
				PlayerShip.fuel--;


				globalvar.Jumped = true;
				Console.WriteLine("Jump Complete");
				Console.WriteLine();
				util.wait(200);

				TheSpaceGame2.EventHandler.RandomGen();
			}


		}

		public static void explore()
		{
			MapGen.CreateStarChart();
			Console.ReadKey();
		}

		public static void Scan(int x, int y)
		{

		}


	}

}

