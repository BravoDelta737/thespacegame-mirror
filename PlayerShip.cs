﻿using System;
using TheSpaceGame2;
using TheSpaceGame;

namespace TheSpaceGame2
{
	
		public partial class PlayerShip //Used to create the playership values used throughout the game
		{
			public static int health = 5; //ship health
			public string name;
			public static int fuel = 10; //amount of fuel, used for jumping
			public static double shieldsCurrent = 0; //shield health
			public static int shieldsTotalStrength =25;
			public static int xlocation = 1; //x location on the world grid
			public static int ylocation = 1;//y location on the world grid
			public static int torpedocount = 10;//number of torpedos. We kinda fogot about this, didn't we?
			public static int torpedodamage = RandomNumGen.numberGen.Next(1,11);//gives a random damage value to the torpedos.
			public static int phaserdamage = RandomNumGen.numberGen.Next (1,7);//gives phasers a random damage value.
		}
	}


