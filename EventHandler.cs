﻿using System;
using TheSpaceGame;

namespace TheSpaceGame2
{
	partial class EventHandler //Used to generate and create  random events
	{
		public static void RandomGen()//The core of the event handling. Contains the primary switch statement.
		{
			//Establishes number generator
			Random gen = new Random();
			int Event = gen.Next(0, 101);//Generates a random number between 1 and 50.

			if (Event <= 15) {
				Asteroid ();	
			} else if (Event <= 50 && Event > 15) {
				Combat.CombatLoop ();
			} else {
				Console.WriteLine ("Nothing Happens");
			}



		}
		public static void Asteroid()//The asteroid event. Roughy a 75% chance of dodging the asteroid, 20% chance of it glancing off, and 5% chance of getting hit directly. Roughly. I think.
		{
			//Random generation setup
			Random gen = new Random();
			int chance = gen.Next(0, 101);//0 - 100 random number generator
			Console.WriteLine("An asteroid is flying directly for your ship!");

			if (chance <= 75)//if less than or equal to 75
			{
				Console.WriteLine("Asteroid Dodged.");//basically nothing happens
				Console.WriteLine();
			}
			else if (chance >= 95)//if you're between 76 and 95
			{
				int dam = gen.Next(1, 11);//generates a random damage value between 1 and 10

				if (PlayerShip.shieldsCurrent == 0)//if player shields are down, damages health
				{
					PlayerShip.health = PlayerShip.health - dam;//SUBTRACTION SWAGACTION
				}
				else//else, your shields are up, so it directs damage to shields.
				{
					PlayerShip.shieldsCurrent = PlayerShip.shieldsCurrent - dam;
				}
				Console.WriteLine("Asteroid glanced off.");
				Console.WriteLine();
			}
			//does the exact same as the above, except with larger damage values, for a direct hit
			else
			{
				int dam = gen.Next(25, 51);
				if (PlayerShip.shieldsCurrent == 0)
				{
					PlayerShip.health = PlayerShip.health - dam;
				}
				else
				{
					PlayerShip.shieldsCurrent = PlayerShip.shieldsCurrent - dam;
				}
				Console.WriteLine("Asteroid hit directly.");
				Console.WriteLine();
			}

		}
	}

}

