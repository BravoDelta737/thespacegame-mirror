﻿using System;
using TheSpaceGame;

namespace TheSpaceGame2
{
	public partial class Combat {
		public static EnemyShip enemyShip = new EnemyShip ();

		public static Boolean checkPlayerShields () {
			if (PlayerShip.shieldsCurrent > 0) {
				return true;
			} else
				return false;
		}

		public static Boolean checkEnemyShields () {
			if (enemyShip.shieldsCurrent > 0) {
				return true;
			} else {
				return false;
			}
		}

		public static Boolean damageChance () {
			Random randomGen = new Random ();
			int numberGen = randomGen.Next (1, 101);

			if (numberGen < 25) {
				return false;
			} else {
				return true;

			}
		}

		public static double calculatePercentage (double shieldsCurrent, int shieldsTotal) {
			double shieldPercentage = (shieldsCurrent / shieldsTotal) * 100;
			return shieldPercentage;
		}

		public static void EnemyTurn () {
			Console.WriteLine ("It's your opponets turn");
			int weponAllowence = 2;
			while (weponAllowence != 0 && PlayerShip.health > 0 && enemyShip.healthCurrent > 0) {

				Random numGen = new Random ();
				int weponChoice = numGen.Next (1,2); 
				Console.Write (".");
				util.wait (1000);
				Console.Write (".");
				util.wait (1000);
				Console.Write (".");
				util.wait (500);

				switch (weponChoice) { //Controls wether enemy fires phasers or torpedos or other special weapons
				case 1: 
					Console.WriteLine ();
					Console.WriteLine ("Armed torpedo detected!");
					util.wait (2000);
					bool chance = damageChance ();

					if (chance) {
						bool playerShield = checkPlayerShields ();

						if (playerShield) {
							PlayerShip.shieldsCurrent = PlayerShip.shieldsCurrent - enemyShip.torpedodamage;
							weponAllowence--;
							Console.WriteLine ();
							Console.WriteLine ("Torpedo hit!");
							Console.WriteLine ();
							Console.WriteLine ("Shields down to " + calculatePercentage(PlayerShip.shieldsCurrent, PlayerShip.shieldsTotalStrength)+ "%");
							Console.WriteLine (PlayerShip.shieldsCurrent);
						} else {
							PlayerShip.health = PlayerShip.health - enemyShip.torpedodamage;
							weponAllowence--;
							Console.WriteLine ();
							Console.WriteLine ("Direct hit!");
							Console.WriteLine ();
							Console.WriteLine ("Hull integrity down to " + PlayerShip.health + "%");
							checkDeath ();
						}
					} else {
						Console.WriteLine ("Enemy Torpedo Missed!");
						weponAllowence--;
					}
					break;

				case 2: 
					Console.WriteLine ();
					Console.WriteLine ("Enemy is charging phasers!");
					util.wait (2000);
					bool Chance = damageChance ();

				break;
				}


			}
		}








		public static void PlayerTurn () {
			Console.WriteLine ("It's your turn");
			int weponAllowence = 2;
			while (weponAllowence != 0 && PlayerShip.health > 0 && enemyShip.healthCurrent > 0) {
				Console.WriteLine ();
				Console.WriteLine ("Awaiting your orders");
				string playerChoice = Console.ReadLine ();

				switch (playerChoice) {
				case "fire torpedo":
					Console.WriteLine ("You have " + PlayerShip.torpedocount + " torpedos left");
					bool chance = damageChance ();
					if (chance) {
						bool enShield = checkEnemyShields ();

						if (enShield) {
							enemyShip.shieldsCurrent = enemyShip.shieldsCurrent - PlayerShip.torpedodamage;
							Console.WriteLine ("Hit! Enemy shields are now at " + calculatePercentage(enemyShip.shieldsCurrent, enemyShip.shieldsTotal)+ "%");
							PlayerShip.torpedocount--;
							weponAllowence--;
						} else {
							enemyShip.healthCurrent = enemyShip.healthCurrent - PlayerShip.torpedodamage;
							Console.WriteLine ("Direct hit! Enemy hull integrity down to " + calculatePercentage(enemyShip.healthCurrent, enemyShip.healthTotal) + "%");
							PlayerShip.torpedocount--;
							weponAllowence--;
						}
					} else {
						Console.WriteLine ("Torpedo missed!");
						PlayerShip.torpedocount--;
						weponAllowence--;
					}
					break;
				case "torpedo count":
					Console.WriteLine ("Torpedo manifest: " + PlayerShip.torpedocount);
					break;

				case "fire phasers":
					Console.WriteLine ("Charging phasers");
					util.loading ();
					bool Chance = damageChance ();
					if (Chance) {
						bool enShield = checkEnemyShields ();

						if (enShield) {
							enemyShip.shieldsCurrent = enemyShip.shieldsCurrent - PlayerShip.phaserdamage;
							Console.WriteLine ("Hit! Enemy shields are now at " + calculatePercentage(enemyShip.shieldsCurrent, enemyShip.shieldsTotal) + "%");
							weponAllowence--;
						} else {
							enemyShip.healthCurrent = enemyShip.healthCurrent - PlayerShip.phaserdamage;
							Console.WriteLine ("Direct hit! Enemy hull integrity down to " + calculatePercentage(enemyShip.healthCurrent, enemyShip.healthTotal) + "%");
							weponAllowence--;
						}
					} else {
						Console.WriteLine ("Phasers missed!");
						weponAllowence--;
					}
					break;
				}
			}
			Console.WriteLine ("End turn");
		}

		public static bool checkDeath () { //change func so it accepts an input of health value. This will make it more flexable
			if (PlayerShip.health <= 0 || enemyShip.healthCurrent <= 0) {
				return false;
			} else {
				return true;
			}
		}

		public static bool checkHealth () {
			if (PlayerShip.health > 0 && enemyShip.healthCurrent > 0) {
				return true;
			} else {
				return false;
			}
		}

		public static void CombatLoop () {
			Console.WriteLine ("RED ALERT! Enemy ship approaching. Full power to wepons and shields");
			enemyShip.healthCurrent = 25;
			enemyShip.healthTotal = 25;
			enemyShip.shieldsCurrent = 15;
			enemyShip.shieldsTotal = 15;
			enemyShip.torpedocount = 10;
			enemyShip.torpedodamage = RandomNumGen.numberGen.Next (1,8);

			//while (checkDeath ()) {
				//if (checkHealth ()) {
					PlayerTurn ();
					//if (checkHealth ()) {
						EnemyTurn ();
					//} else {
						//Console.WriteLine ("Enemy ship destroied!");
					//}
				//} else {
					//Console.WriteLine ("Game Over");
				//}



			}
		}
	}

	


